/**
 * Created by Akarshan on 6/9/2015.
 */
(function(){
    "use strict";
    angular
        .module('app', [
        'ngRoute',
        'ngMessages',
        'ngResource',
        'ngAnimate'
    ]);

    angular.module('app').config(function($routeProvider){
        $routeProvider
            .when('/',
            {
                controller:'mainctrl',
                templateUrl:'/HRS/views/home.html',
                controllerAs: 'vm'
            })
            .when('/newcustomer',
            {
                controller:'custCtrl',
                templateUrl:'/HRS/views/customer/newCustomer.html',
                controllerAs: 'vm'
            })
            .when('/retcustomer',
            {
                controller:'custCtrl',
                templateUrl:'/HRS/views/customer/retCustomer.html',
                controllerAs: 'vm'
            })
            .when('/editreservation',
            {
                controller:'custCtrl',
                templateUrl:'/HRS/views/customer/editReservation.html',
                controllerAs: 'vm'
            })
            .when('/reservations',
            {
                controller:'ownerCtrl',
                templateUrl:'/HRS/views/owner/reservations.html',
                controllerAs: 'vm'
            })
            .when('/readme',
            {
                templateUrl:'/HRS/readme.html'
            })
            .otherwise({redirectTo: '/'});
    });
})();

