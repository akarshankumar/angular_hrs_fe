/**
 * Created by Akarshan on 6/10/2015.
 */
(function(){
    "use strict";
    angular.module('app').factory('resDataFactory', resDataFactory);

    var data = {
        custData:[],
        resData:[]
    };
    function resDataFactory(){
        //status.value = false;
        return data;
    }
})();