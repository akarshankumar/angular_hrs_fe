/**
 * Created by Akarshan on 6/11/2015.
 */
(function(){
    "use strict";
    angular.module('app').factory('loginStatFactory', loginStatFactory);

    var status = {};
    function loginStatFactory(){
        status.value = false;
        return status;
    }
})();