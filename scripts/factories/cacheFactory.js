/**
 * Created by Akarshan on 6/13/2015.
 */
(function(){
    angular
        .module('app')
        .factory('superCache', ['$cacheFactory',superCache] );

    /* @ngInject */
    function superCache($cacheFactory) {
        return $cacheFactory('super-cache');
    }
})();