/**
 * Created by Akarshan on 6/10/2015.
 */
(function(){
    "use strict";
    angular.module('app').factory('ownCustFactory', ownCustFactory);

     var isOwner = {
         flagVal:false
     };
    function ownCustFactory(){
        isOwner.flagVal = false;
        this.flip = flip;

        function flip(value){
            isOwner.flagVal = value;
        }
        return isOwner;
    }
})();