/**
 * Created by Akarshan on 6/10/2015.
 */

(function (){
    "use strict";
    angular
        .module('app')
        .factory('tableFactory',['$http',tableFactory]);

    /* @ngInject */
    function tableFactory($http) {
        var service = {};
        var urlBase = '/HRS/data/table.json';
        //var urlBase = 'http://localhost:8080/HRSBackend/rs/tablelist';
        service.getTables = function (){return $http.get(urlBase);};
        return service;
    }
})();
