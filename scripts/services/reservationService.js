/**
 * Created by Akarshan on 6/10/2015.
 */

(function (){
    "use strict";
    angular
        .module('app')
        .factory('reservationFactory',['$http',reservationFactory]);

    /* @ngInject */
    function reservationFactory($http) {
        var service = {};
        var urlBase = '/HRS/data/reservation.json';
        //var urlBase = 'http://localhost:8080/HRSBackend/rs/reservation';

        service.getReservations = function (){return $http.get(urlBase);};
        return service;
        }

})();
