/**
 * Created by Akarshan on 6/10/2015.
 */

(function (){
    "use strict";
    angular
        .module('app')
        .factory('customerFactory',['$http',customerFactory]);

    /* @ngInject */
    function customerFactory($http) {
        var service = {};
        var urlbase='/HRS/data/customer.json';
        //var urlbase='http://localhost:8080/HRSBackend/rs/customer';
        service.getCustomers = function (){return $http.get(urlbase)};
        service.insertCustomer = function (obj) {console.log(obj);return $http.post(urlbase, obj);};
        service.updateCustomer = function (obj) {return $http.put(urlbase + '/' + obj.name, obj)};
        service.deleteCustomer = function (name) {return $http.delete(urlbase + '/' + name);};
        return service;
        };
})();
