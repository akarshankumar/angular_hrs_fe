/**
 * Created by Akarshan on 6/10/2015.
 */

(function (){
    "use strict";
    angular
        .module('app')
        .factory('profileFactory',['$http',profileFactory]);

    /* @ngInject */
    function profileFactory($http) {
        var service = {};
        var urlBase = '/HRS/data/profile.json';
        //var urlBase = 'http://localhost:8080/HRSBackend/rs/profile';
        service.getProfile = function (){ return $http.get(urlBase);};
        service.updateProfile = function (obj) {return $http.put(urlBase + '/' + obj.name, obj);};
        return service;
    }


})();
