/**
 * Created by Akarshan on 6/10/2015.
 */
(function(){
"use strict";
    angular
        .module('app')
        .controller('custCtrl', [
            '$filter',
            '$scope',
            '$location',
            '$http',
            '$log',
            'customerFactory',
            '$window',
            'reservationFactory',
            'resStatFactory',
            'resDataFactory',
            'superCache',
            custCtrl]);

//angular
//    .module('app')
//    .controller('custCtrl').$inject = ;

    /* @ngInject */
    function custCtrl($filter,s,l,h,$log,customerFactory,$window,reservationFactory,resStatFactory,resDataFactory,superCache) {
        /* jshint validthis: true */
        var vm = this;
        vm.isOwner = true;
        vm.activate = activate;
        vm.title = 'customerController';
        vm.testCode;
        vm.confirmationCode;
        vm.customername;
        vm.showconfcode = false;
        vm.reservationconfirm = resStatFactory.value;
        vm.reservations=[];
        vm.newcust = {};
        vm.customers =[];
        vm.retCust = resDataFactory.custData;
        vm.confirmReservation = confirmReservation;
        vm.deleteReservation = deleteReservation;
        vm.reserveTable = reserveTable;
        vm.confirmCode = confirmCode;
        vm.editReservation = editReservation;
        vm.changeView = function(view){l.path(view)};

        activate();

        ////////////////

        function activate() {
            readCustomers();
            readReservations();
            console.log('super cache',superCache.get('name'));
        }

        function deleteReservation(){
            customerFactory.deleteCustomer(vm.retCust.name)
                .success(function () {
                    $window.alert('Deleted Customer! Refreshing customer list.');
                    for (var i = 0; i < vm.customers.length; i++) {
                        if (vm.customers[i].name === vm.retCust.name) {
                            vm.customers.splice(i, 1);
                            break;
                        }
                    }
                })
                .error(function (error) {
                    $scope.status = 'Unable to delete customer: ' + error.message;
                });
        }

        function editReservation(){
            customerFactory.updateCustomer(vm.retCust)
                .success(function () {
                    $window.alert('Updated Reservation!');
                })
                .error(function (error) {
                    console.log('Unable to update Reservation: ' + error.message);
                });
        }


        function reserveTable(){
            console.log(vm.newcust);
            vm.newcust.date = new Date(vm.newcust.date);
            //vm.newcust.date = $filter('date')(vm.newcust.date,'mediumDate')
            //vm.newcust.time = $filter('date')(vm.newcust.time,'shortTime')
            console.log(vm.newcust);
            customerFactory.insertCustomer(vm.newcust).success(function () {
                console.log('Inserted Customer! Refreshing customer list.');
                //vm.customers.push(vm.newcust);
                //console.log(vm.newcust);
            }).
                error(function(error) {
                    console.log('Unable to insert customer: ' + error.message);
                });
        }

        function readCustomers()
        {
            var promise = customerFactory.getCustomers();
            promise.then(function(data){
                vm.customers = angular.copy(data.data);
                console.log(vm.customers);
            },function(error){
                console.log('reservationFactory error',error.message);
            });

            customerFactory.getCustomers().success(function(data){
                vm.customers = angular.copy(data);
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }

        function readReservations()
        {
            reservationFactory.getReservations().success(function(data){
                vm.reservations = data;
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }



        function confirmReservation(){

        }

        function confirmCode() {
            if (vm.customername) {
                angular.forEach(vm.customers,function(customer, key){
                    if (customer.name == vm.customername) {
                        vm.showconfcode = true;
                        vm.testCode = customer.confcode;
                        resDataFactory.custData = customer;
                    }
                });
                //for (var i = 0; i < vm.customers.length; i++) {
                //    if (vm.customers[i].name == vm.customername) {
                //        vm.showconfcode = true;
                //        vm.testCode = vm.customers[i].confcode;
                //        resDataFactory.custData = vm.customers[i];
                //    }
                //}
                if(!vm.showconfcode){
                    $window.alert('Incorrect user name')
                }
            } else {
                $window.alert('Please enter an username.');
            }
            if (vm.confirmationCode) {
                if (vm.testCode == vm.confirmationCode) {
                    for (var i = 0; i < vm.reservations.length; i++) {
                        if (vm.reservations[i].customer == vm.customername) {
                            resDataFactory.resData = vm.reservations[i];
                            if (vm.reservations[i].status == 'confirmed') {
                                resStatFactory.value = true;
                                vm.changeView('/editreservation');
                                break;
                            }

                            if (vm.reservations[i].status == 'pending') {
                                resStatFactory.value = false;
                                vm.changeView('/editreservation');
                                break;
                            }

                            if (vm.reservations[i].status == 'waiting') {
                                $window.alert('Reservation not confirmed yet.');
                                vm.customername = null;
                                vm.confirmationCode = null;
                                vm.changeView('/retcustomer');
                                break;
                            }
                        }
                        console.log(vm.reservationconfirm);
                    }
                    //vm.changeView('/editreservation');
                } else {
                    $window.alert('Incorrect code');
                    vm.changeView('/retcustomer');
                }


            }

        }



    }
})();

