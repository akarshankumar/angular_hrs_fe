/**
 * Created by Akarshan on 6/10/2015.
 */

(function(){

    "use strict";
    angular
        .module('app')
        .controller('ownerCtrl',['$scope',
            '$filter',
            '$location',
            '$http',
            '$log',
            'reservationFactory',
            'tableFactory',
            'customerFactory',
            'profileFactory',
            'loginStatFactory',
            'superCache',
            ownerCtrl]);

    /* @ngInject */
    function ownerCtrl(s,$filter,l,h,$log,reservationFactory,tableFactory,customerFactory,profileFactory,loginStatFactory,superCache) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'ownerController';
        vm.reservations=[];
        vm.tables=[];
        vm.customers =[];
        vm.profile;
        vm.customerDetails = customerDetails;
        vm.reservationDetails = reservationDetails;
        vm.reserveTable = reserveTable;
        vm.editReservation = editReservation;
        vm.profileUpdate = profileUpdate;
        this.changeView = function(view){l.path(view)};
        vm.logout = function(){loginStatFactory.status = false; vm.secure = false;this.changeView('/');};
        vm.checkbx = function(){console.log(vm.profile.autoassign);};
        superCache.put('name','Akarshan');

        activate();

        ////////////////

        function activate() {
            if(loginStatFactory.status){
                vm.secure = true;
                readReservations();
                readTables();
                readCustomers();
                readProfile();
                var test = superCache.get('name');
                console.log('super cache',test);
            } else {
                vm.secure = false;
            }
        }

        function profileUpdate(){
            profileFactory.updateProfile(vm.profile)
                .success(function () {
                    console.log('Updated Customer! Refreshing customer list.');
                })
                .error(function (error) {
                    console.log('Unable to update customer: ' + error.message);
                });
        }

        function readProfile()
        {
            profileFactory.getProfile().success(function(data){
                vm.profile = data[0];
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }

        function readCustomers()
        {
            customerFactory.getCustomers().success(function(custdata){
                vm.customers = custdata;;
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }

        function readTables(){
            tableFactory.getTables().success(function(tabledata){
                vm.tables = tabledata;
            }).error(function(error){
                console.log('tableFactory error',error.message);
            });
        }

        function readReservations()
        {
            reservationFactory.getReservations().success(function(resdata){
                vm.reservations = resdata;
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }


        function customerDetails(index){
            $log.log('Customer Details ' + index + ' ' + vm.tables[index].confcode);
            for(var i = 0; i < vm.customers.length;i++){
                if(vm.customers[i].confcode == vm.tables[index].confcode){
                    vm.customer = vm.customers[i];
                    //vm.customer.date = new Date(vm.customer.date);
                    vm.customer.date = $filter('date')(vm.customer.date,'shortDate');
                    console.log(vm.customer);
                }
            }
        }

        function reservationDetails(index){
            this.reservation = this.reservations[index];
        }

        function reserveTable(){
            window.alert('Logic to allocate table to customer if available or else, tag it as pending for owners attention');
        }

        function editReservation(){
            window.alert('Logic to edit reservation if it is confirmed.');
        }



    }
})();
