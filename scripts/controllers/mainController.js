/**
 * Created by Akarshan on 6/9/2015.
 */
(function () {
    "use strict";
    console.log('Inside mainController');
    angular
        .module('app')
        .controller('mainctrl',['$scope','$location','$http','$log','ownCustFactory','profileFactory','loginStatFactory',mainctrl]);
        //.$inject = ['$scope','$location','$http','$log'];

    /* @ngInject */
    function mainctrl(s,l,h,$log,ownCustFactory,profileFactory,loginStatFactory) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'MainController';
        vm.isOwner = ownCustFactory.flagVal;
        vm.ownerAuth = ownerAuth;
        vm.owner = {};
        this.changeView = function(view){l.path(view)};
        //vm.backendOwner = profileFactory.query();
        vm.cred = {};

        vm.profile =[];

        function readProfile()
        {
            profileFactory.getProfile().success(function(data){
                vm.profile = data;
            }).error(function(error){
                console.log('reservationFactory error',error.message);
            });
        }

        function ownerAuth(){
            console.log('Authenticating');
            vm.cred.username=vm.profile[0].username;
            vm.cred.password=vm.profile[0].password;

            if((vm.owner.username == vm.cred.username)&&(vm.owner.password == vm.cred.password)){
                loginStatFactory.status = true;
                l.path('/reservations');
            } else {
                console.log('no match');
            }

        }
        activate();

        ////////////////

        function activate() {
            readProfile();
        }


    }
})();