/**
 * Created by Akarshan on 6/10/2015.
 */

(function(){
    "use strict";
    angular
        .module('app')
        .controller('navCtrl',['$scope','$location','$http','$log','ownCustFactory',navCtrl]);

    function navCtrl(s,l,h,$log,ownCustFactory) {
        /* jshint validthis: true */
        var vm = this;

        vm.activate = activate;
        vm.title = 'NavigationController';
        this.changeView = function(view){l.path(view)};
        this.isOwner = ownCustFactory.flagVal;
        this.flip = flip;

        function flip(value){
            ownCustFactory.flagVal = value;
            this.isOwner = ownCustFactory.flagVal;
        }

        activate();

        ////////////////

        function activate() {
        }


    }
})();
